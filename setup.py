#!/usr/bin/env python

from distutils.core import setup

setup(name='example_colormaps',
      version='0.0.1',
      description='Example colormaps of future matplotlib version',
      author='Tuomas Karna',
      author_email='tuomas.karna@gmail.com',
      url='https://bitbucket.org/tkarna/example_colormaps',
      packages=['example_colormaps'],
      )

